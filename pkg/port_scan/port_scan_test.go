package port_scan

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/devinturner/lib/pkg/net_helper"
	"math/rand"
	"net"
	"os"
	"testing"
	"time"
)

// ResolveTCPAddr will have the OS pick an open port when provided with 0
func mockTCPAddr(t *testing.T) *net.TCPAddr {
	addr, err := net.ResolveTCPAddr("tcp", ":0")
	if err != nil {
		t.Fatal(err)
	}
	return addr
}

func mockListener(t *testing.T) net.Listener {
	addr := mockTCPAddr(t)

	l, err := net.Listen("tcp", addr.String())
	if err != nil {
		t.Fatal(err)
	}
	return l
}

func TestIsListening(t *testing.T) {
	l := mockListener(t)
	defer l.Close()

	_, err := net_helper.GetPortFromAddress(l.Addr().String())
	if err != nil {
		t.Fatal(err)
	}

	invalidPort, err := net_helper.GetPortFromAddress(mockTCPAddr(t).String())
	if err != nil {
		t.Fatal(err)
	}
	assert.True(t, Scan(l.Addr().String()))
	assert.False(t, Scan(fmt.Sprintf(":%d", invalidPort)))
}

func TestScanPortsBroadcastUnbuffered(t *testing.T) {
	openListeners := GenerateListeners(t, 10)
	openPorts := make([]int, 0)
	for _, l := range openListeners {
		port, err := net_helper.GetPortFromAddress(l.Addr().String())
		if err != nil {
			t.Fatal(err)
		}
		openPorts = append(openPorts, port)
	}

	// Closed listeners are explicitly closed prior to ScanPortsBroadcast
	closedListeners := GenerateListeners(t, 10)
	closedPorts := make([]int, 0)
	for _, l := range closedListeners {
		port, err := net_helper.GetPortFromAddress(l.Addr().String())
		if err != nil {
			t.Fatal(err)
		}
		closedPorts = append(closedPorts, port)
		l.Close()
	}


	queryPorts := append(closedPorts, openPorts...)
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(queryPorts), func(i, j int) { queryPorts[i], queryPorts[j] = queryPorts[j], queryPorts[i] })

	actualPorts := make([]int, 0)
	portChan := make(chan int)

	t.Logf("scanning ports: %v", queryPorts)
	go ScanPortsBroadcast(portChan, "", queryPorts)
	for {
		port, ok := <-portChan
		if !ok {
			break
		}
		actualPorts = append(actualPorts, port)
	}
	t.Logf("open ports: %v", actualPorts)
	assert.ElementsMatch(t, openPorts, actualPorts)
}

func TestScanPortsBroadcastIntegrationBuffered(t *testing.T) {
	if os.Getenv("CI") != "true" {
		t.Skip("skip integration test when not in CI environment")
	}
	openPorts := []int{ 22, 80 }
	queryPorts := make([]int, 0)
	for i := 1; i <= 1024; i++ {
		queryPorts = append(queryPorts, i)
	}

	actualPorts := make([]int, 0)
	portChan := make(chan int, 100)

	go ScanPortsBroadcast(portChan, "scanme.nmap.org", queryPorts)
	for {
		port, ok := <-portChan
		if !ok {
			break
		}
		actualPorts = append(actualPorts, port)
	}
	t.Logf("open ports: %v", actualPorts)
	assert.ElementsMatch(t, openPorts, actualPorts)

}

func GenerateListeners(t *testing.T, n int) []net.Listener {
	l := make([]net.Listener, 0)
	for i := 0; i < n; i++ {
		l = append(l, mockListener(t))
	}
	return l
}

