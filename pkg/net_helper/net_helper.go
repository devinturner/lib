package net_helper

import (
	"net"
	"strconv"
)

func GetPortFromAddress(addr string) (int, error) {
	_, rawPort, err := net.SplitHostPort(addr)
	if err != nil {
		return 0, err
	}
	return strconv.Atoi(rawPort)
}
