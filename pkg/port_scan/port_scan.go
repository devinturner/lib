package port_scan

import (
	"fmt"
	"net"
	"sync"
	"time"
)

const DefaultTimeout = time.Second * 10

func Scan(address string) bool {
	return ScanTimeout(address, DefaultTimeout)
}

func ScanTimeout(address string, timeout time.Duration) bool {
	conn, err := net.DialTimeout("tcp", address, timeout)
	if err != nil {
		return false
	}
	conn.Close()
	return true
}

// NOTE the size of the buffer will determine the number of concurrent requests sent
func ScanPortsBroadcast(ch chan int, host string, ports []int) {
	var wg sync.WaitGroup
	wg.Add(len(ports))

	for _, port := range ports {
		addr := fmt.Sprintf("%s:%d", host, port)
		go func(p int) {
			defer wg.Done()
			if Scan(addr) {
				ch <- p
			}
		}(port)
	}
	wg.Wait()
	close(ch)
}

